const companies= [
    {name: "Company One", category: "Finance", start: 1981, end: 2004},
    {name: "Company Two", category: "Retail", start: 1992, end: 2008},
    {name: "Company Three", category: "Auto", start: 1999, end: 2007},
    {name: "Company Four", category: "Retail", start: 1989, end: 2010},
    {name: "Company Five", category: "Technology", start: 2009, end: 2014},
    {name: "Company Six", category: "Finance", start: 1987, end: 2010},
    {name: "Company Seven", category: "Auto", start: 1986, end: 1996},
    {name: "Company Eight", category: "Technology", start: 2011, end: 2016},
    {name: "Company Nine", category: "Retail", start: 1981, end: 1989}
  ];
  
  const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];
  companies.forEach(function(company) {
    console.log(company.name);
  });
  let canDrink=[];
  for(let i=0; i < ages.length; i++) {
    if(ages[i] >=21) {
      canDrink.push(ages[i]);
    }
  }
   canDrink =ages.filter(function(age) {
    if(age >= 21) {
      return true;
    }
  });
 canDrink=ages.filter(age => age >= 21);

  Companies=companies.filter(company => company.category === 'Retail');
  const eightiesCompanies=companies.filter(company => (company.start >= 1980 && company.start < 1990));
  const lastedTenYears=companies.filter(company => (company.end - company.start >= 10));

  const companyNames=companies.map(function(company) {
    return company.name;
  });
  let testMap=companies.map(function(company) {
    return `${company.name} [${company.start} - ${company.end}]`;
  });
   testMap=companies.map(company => `${company.name} [${company.start} - ${company.end}]`);
  const ageMap = ages
    .map(age => Math.sqrt(age))
    .map(age => age * 2);
  var sortedCompanies=companies.sort(function(c1, c2) {
    if(c1.start > c2.start) {
      return 1;
    } else {
      return -1;
    }
  });
  var sortedCompanies=companies.sort((a, b) => (a.start > b.start ? 1 : -1));

  const sortAges=ages.sort((a, b) => a - b);
  console.log(sortAges);
  var age_Sum=0;
  for(let i = 0; i < ages.length; i++) {
    age_Sum +=ages[i];
  }
  var age_Sum=ages.reduce(function(total, age) {
    return total + age;
  }, 0);

  var age_Sum=ages.reduce((total, age) => total + age, 0);
  var totalYears=companies.reduce(function(total, company) {
    return total + (company.end - company.start);
  }, 0);

  var totalYears=companies.reduce((total, company) => total + (company.end - company.start), 0);
  const combined=ages
    .map(age => age * 2)
    .filter(age => age >= 40)
    .sort((a, b) => a - b)
    .reduce((a, b) => a + b, 0);
  
  console.log(combined);
  